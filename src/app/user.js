module.exports = new class {
    constructor() {
    
    }
    
    get() {
        return JSON.parse(localStorage.getItem('user'));
    }
    
    logout() {
        localStorage.removeItem('user');
        localStorage.removeItem('session');
        return router.push('/auth')
    }
};
